const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function addBookToList(book) {

    try {
        if (!book.author || !book.name || !book.price) {
            throw new Error('Invalid book');
        }
        const rootElement = document.getElementById('root');
        const ul = document.createElement('ul');
        const li = document.createElement('li');
        li.textContent = `${book.author}: ${book.name} - $${book.price}`;
        ul.appendChild(li);
        rootElement.appendChild(ul);
    } catch (error) {
        console.error(error.message);
    }
}

books.forEach(addBookToList);